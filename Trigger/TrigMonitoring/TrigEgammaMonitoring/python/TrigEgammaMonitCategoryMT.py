# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration




#Categories currently used by offline Egamma TO monitoringMT tool



monitoring_photon = [
        'HLT_g35_medium_L1EM20VH',
        'HLT_g35_loose_L1EM22VHI', 
        'HLT_g35_tight_L1EM22VHI',
        'HLT_g35_tight_icaloloose_L1EM22VHI',
        'HLT_g20_loose_L1EM15VH'
        ]

monitoring_electron = [
        'HLT_e5_etcut_L1EM3',
        'HLT_e5_lhtight_L1EM3'
        ]

monitoringTP_electron = [
        'HLT_e17_lhvloose_L1EM15VHI',
        'HLT_e26_dnnloose_L1EM22VHI',
        'HLT_e26_dnnmedium_L1EM22VHI',
        'HLT_e26_dnntight_L1EM22VHI',
        'HLT_e26_lhtight_L1EM22VHI',
        'HLT_e26_lhtight_ivarloose_L1EM22VHI',
        'HLT_e26_lhtight_ivarmedium_L1EM22VHI',
        ]

validation_photon = [
        'HLT_g20_loose_L1EM15VHI',
        'HLT_g20_medium_L1EM15VHI',
        'HLT_g20_tight_L1EM15VHI',
        'HLT_g20_tight_icalotight_L1EM15VHI',
        'HLT_g20_tight_icalomedium_L1EM15VHI',
        'HLT_g20_tight_icaloloose_L1EM15VHI',
        'HLT_g25_etcut_L1EM20VH',
        'HLT_g25_loose_L1EM20VH',
        'HLT_g35_medium_L1EM22VHI',
        'HLT_g120_loose_L1EM22VHI',
        'HLT_g140_etcut_L1EM22VHI'
        ]

validation_electron = [
        'HLT_e5_etcut_L1EM3',
        'HLT_e5_lhtight_noringer_L1EM3',
        'HLT_e5_lhtight_gsf_L1EM3'
        ]

validationTP_electron = [
        'HLT_e12_lhvloose_L1EM10VH',
        'HLT_e17_lhvloose_L1EM15VHI',
        'HLT_e17_lhvloose_gsf_L1EM15VHI',
        'HLT_e26_lhloose_L1EM15VH',
        'HLT_e26_lhmedium_L1EM15VH',
        'HLT_e26_lhtight_L1EM15VH',
        'HLT_e26_lhtight_ivarloose_L1EM15VH',
        'HLT_e26_lhtight_ivarmedium_L1EM15VH',
        'HLT_e26_lhtight_ivartight_L1EM15VH',
        'HLT_e26_lhtight_L1EM22VHI',
        'HLT_e26_lhtight_gsf_L1EM22VHI',
        'HLT_e26_lhtight_gsf_ivarloose_L1EM22VHI',
        'HLT_e26_lhtight_ivarloose_L1EM22VHI',
        'HLT_e26_lhtight_ivarmedium_L1EM22VHI',
        'HLT_e26_lhtight_ivartight_L1EM22VHI',
        'HLT_e60_lhmedium_nod0_L1EM22VHI',
        'HLT_e140_lhloose_nod0_L1EM22VHI'
        ]

validation_jpsi = [
        'HLT_e9_lhtight_e4_etcut_Jpsiee_L1JPSI-1M5-EM7',
        'HLT_e5_lhtight_e9_etcut_Jpsiee_L1JPSI-1M5-EM7',
        'HLT_e14_lhtight_e4_etcut_Jpsiee_L1JPSI-1M5-EM12',
        'HLT_e5_lhtight_e14_etcut_Jpsiee_L1JPSI-1M5-EM12',
        'HLT_e9_lhtight_noringer_e4_etcut_Jpsiee_L1JPSI-1M5-EM7',
        'HLT_e5_lhtight_noringer_e9_etcut_Jpsiee_L1JPSI-1M5-EM7',
        'HLT_e14_lhtight_noringer_e4_etcut_Jpsiee_L1JPSI-1M5-EM12',
        'HLT_e5_lhtight_noringer_e14_etcut_Jpsiee_L1JPSI-1M5-EM12'
        ]

validationTP_jpsiee = ['HLT_e5_lhtight_L1EM3']


validation_Zee = [
        'HLT_e26_lhtight_e15_etcut_Zee_L1EM22VHI', 
        'HLT_e26_lhtight_e15_etcut_idperf_Zee_L1EM22VHI', 
        'HLT_e20_lhtight_ivarloose_L1ZAFB-25DPHI-eEM18I'
        ] # adding Zee/special chains later      
